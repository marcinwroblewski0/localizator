<?php

    if(!isset($_SESSION)) session_start();
    session_regenerate_id();
    if(isset($_POST["sid"])) {
        session_id($_POST["sid"]);
    }
    if(!isset($_SESSION["loggedIn"]) || !$_SESSION["loggedIn"]) {
        http_response_code(401);
        echo(json_encode(array('error' => "Not logged in")));
        exit();
    }

    require_once "SDM.php";
    require_once "db.php";

	$carName = $_POST['name'];

    $sdm = new SDM($db_address, $db_user, $db_password, $db_name);

    echo(json_encode(array('sql_response' => $sdm->delete('cars', 'name="'.$carName.'"'), 'carName' => $carName )));

    $sdm->jobDone();
?>
