<?php
     
    if(isset($_POST["sid"])) {
        session_id($_POST["sid"]);
    }
    if(!isset($_SESSION)) session_start();

    if(!isset($_SESSION["loggedIn"]) || !$_SESSION["loggedIn"]) {
        http_response_code(200);
        echo(json_encode(array('error' => "Not logged in")));
        
        exit();
    }



    require_once "SDM.php";
    require_once "car.php";
    require_once "location.php";
    require_once "db.php";

    
    $sdm = new SDM($db_address, $db_user, $db_password, $db_name);
    $allCars = $sdm->select("cars", "*", null);

    //echo(json_encode($allCars));    

    $allCarsJSON = array();



    foreach ($allCars as $carId => $carInfo) {
        $gauge;

        if($carInfo['max_capacity'] < 500) {
            $gauge = "Samochód dostawczy";
        } else if($carInfo['max_capacity'] > 500 && $carInfo['max_capacity'] < 1000) {
            $gauge = "Samochód ciężarowy";
        } else if($carInfo['max_capacity'] > 1000) {
            $gauge = "Ciągnik z naczepą";
        } 

        $driverId = $carInfo["driver"];

        $driverName;
        if($driverId != 0)
            $driverName = $sdm->select("users", "name", "id=".$driverId)[0]["name"];
        else 
            $driverName = "Brak";


        $allCarsJSON[$carId] = json_encode(
            array('name' => $carInfo['name'], 
            'max_capacity' => $carInfo['max_capacity'],
            'current_capacity' => $carInfo['current_capacity'],
            'gauge' => $gauge,
            'longtitude' => $carInfo['longtitude'],
            'langtitude' => $carInfo['langtitude'],
            'state' => $carInfo["state"],
            'driver' => "(".$driverId.") ".$driverName, 
            'current_destination' => $carInfo["current_destination"],
            'requested_destination' => $carInfo["requested_destination"]));

    }
    $sdm->jobDone();
    echo(json_encode($allCarsJSON));
?>