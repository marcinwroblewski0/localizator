<?php

    if(!isset($_SESSION)) session_start();
    session_regenerate_id();
    if(isset($_POST["sid"])) {
        session_id($_POST["sid"]);
    }
    
    if(!isset($_SESSION["loggedIn"]) || !$_SESSION["loggedIn"]) {
        http_response_code(401);
        echo(json_encode(array('error' => "Not logged in")));
        exit();
    }

    require_once "SDM.php";
    require_once "car.php";
    require_once "location.php";
    require_once "db.php";


    if(isset($_POST['longtitude'])) {

        $sdm = new SDM($db_address, $db_user, $db_password, $db_name);

        echo $sdm->update("cars", "longtitude", $_POST['longtitude'], "name='".$_POST["car_name"]."'");

        echo $sdm->update("cars", "langtitude", $_POST['langtitude'], "name='".$_POST["car_name"]."'");
        $sdm->jobDone();

        echo("ok");
    }

?>

<form method="post">

    <input type="text" name="car_name"/>
    <input type="text" name="longtitude"/>
    <input type="text" name="langtitude"/>

    <input type="submit"/>
</form>