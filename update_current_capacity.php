<?php

    if(!isset($_SESSION)) session_start();
    session_regenerate_id();
    if(isset($_POST["sid"])) {
        session_id($_POST["sid"]);
    }
    if(!isset($_SESSION["loggedIn"]) || !$_SESSION["loggedIn"]) {
        http_response_code(401);
        echo(json_encode(array('error' => "Not logged in")));
        exit();
    }

    require_once "SDM.php";
    require_once "db.php";

    if(isset($_POST['current_capacity'])) {
        $sdm = new SDM($db_address, $db_user, $db_password, $db_name);

        echo $sdm->update("cars", "current_capacity", $_POST['current_capacity'], "name='".$_POST["car_name"]."'");
        $sdm->jobDone();

        echo("ok");
    }

?>

<form method="post">

    <input type="text" name="car_name"/>
    <input type="number" name="current_capacity"/>

    <input type="submit"/>
</form>