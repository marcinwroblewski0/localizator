
-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Czas wygenerowania: 17 Sie 2016, 11:27
-- Wersja serwera: 10.0.20-MariaDB
-- Wersja PHP: 5.2.17

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Baza danych: `u533700197_local`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `cars`
--

CREATE TABLE IF NOT EXISTS `cars` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text COLLATE utf8_bin NOT NULL,
  `max_capacity` int(11) NOT NULL,
  `current_capacity` int(11) NOT NULL,
  `longtitude` double NOT NULL,
  `langtitude` double NOT NULL,
  `state` int(11) NOT NULL,
  `current_destination` text COLLATE utf8_bin NOT NULL,
  `requested_destination` text COLLATE utf8_bin NOT NULL,
  `driver` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=16 ;

--
-- Zrzut danych tabeli `cars`
--

INSERT INTO `cars` (`id`, `name`, `max_capacity`, `current_capacity`, `longtitude`, `langtitude`, `state`, `current_destination`, `requested_destination`, `driver`) VALUES
(13, 'Citroën Nemo', 430, 430, 19.039993, 50.270908, 0, '', 'Sosnowiec, Polska', 0),
(10, 'Mercedes Sprinter', 1120, 99, 18.9261603, 50.2627289, 1, 'Ruda Śląska Plaza, 1 Maja, Ruda Śląska', '', 5),
(11, 'MAN TGX', 2500, 1500, 19.0017184, 50.2510755, 3, 'Karoliny 4, 40-186 Katowice, Polska', '', 4),
(12, 'VW Caddy', 650, 325, 19.1232273, 50.277295, 2, '', '', 2),
(15, 'Leon', 10, 10, 19.039993, 50.270908, 0, '', 'Zakopane, Polska', 0);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `roles`
--

CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=4 ;

--
-- Zrzut danych tabeli `roles`
--

INSERT INTO `roles` (`id`, `name`) VALUES
(1, 'ADMIN'),
(2, 'DRIVER'),
(3, 'VIEWER');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `states`
--

CREATE TABLE IF NOT EXISTS `states` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=4 ;

--
-- Zrzut danych tabeli `states`
--

INSERT INTO `states` (`id`, `name`) VALUES
(0, 'Wolny'),
(1, 'W drodze'),
(2, 'Załadunek/Rozładunek'),
(3, 'W drodze do bazy');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` text COLLATE utf8_bin NOT NULL,
  `password` text COLLATE utf8_bin NOT NULL,
  `name` text COLLATE utf8_bin NOT NULL,
  `role` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=7 ;

--
-- Zrzut danych tabeli `users`
--

INSERT INTO `users` (`id`, `login`, `password`, `name`, `role`) VALUES
(1, 'admin', '$2y$10$HnoBv6VYU7IxOHTNONbJaOArbJSUANA5yYt/ylpH2ejSyeVlMVR36', 'Administrator', 1),
(2, 'kierowca', '$2y$10$s9uiNYUWIQqjWlHZeowJmOUAS2gpB0L4eJ1AIuvhsDVARrlaho9uq', 'Jan Kowalski', 2),
(3, 'bhalska', '$2a$04$garlkMzpSpU65FBi2NHO.eRux.Lt7Jnc0ZnIBP1T5sNrxBSqTmpqW', 'Barbara Halska', 3),
(4, 'kierowca2', '$2a$04$cFfD1JVOLrPliTacjPN8vu0OtEeQRS7hl1wYmXu0sgeFwetxOoPNG', 'Paweł Korzeniowski', 2),
(5, 'kierowca3', '$2a$04$5bm9D4VkxEgeN4ANfR6fAOksLIXxR3BVWfb0Oy2zwdiC51mml9rjq', 'Michał Zając', 2),
(6, 'bhalska2', '$2y$10$c61e2VHcWlWUABbUoG.LIOdGmqcIQXrfENzmDJ61TVtUwtEW8Case', '', 3);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
