<?php

    //@1mXJ*Xh#``x6Tp*;J

   class SDM {
       
       private $_db;
       
       public function __construct($address, $user, $password, $name) {
           $this->_db = new mysqli($address, $user, $password, $name);
           
           if(!$this->_db) {
               echo("Cannot connect to MySQL".PHP_EOL);
               echo("Error #".mysqli_connect_errno().PHP_EOL);
               exit();
           } 
          
       }
       
       public function select($table, $param, $condition) {

           if($condition == ""){
                $result = $this->_db->query("SELECT ".$param." from ".$table);
           } else {
                $result = $this->_db->query("SELECT ".$param." from ".$table." WHERE ".$condition);
           }


           if(!$result) {
               return null;
               exit();
           }
           
           $array = array();
          
           $i = 0;
           while($row = $result->fetch_assoc()) {
                $array[$i] = $row;
                $i++;
           }
           
           return $array;
       }
       
       public function insert($table, $data) {
           $keys = "("; $values = "(";
           
           foreach ($data as $key => $value) {
              $keys .= $key.", ";
              $values .= "'".$value."', ";
           }
           
           $keys = substr($keys, 0, -2).")";
           $values = substr($values, 0, -2).")";
           
           $sql = "INSERT INTO ".$table." ".$keys." VALUES ".$values;
           
           return $this->_db->query($sql);
       }
       
       public function delete($table, $condition) {
           $sql = "DELETE FROM ".$table." WHERE ".$condition;
           
           return $this->_db->query($sql);
       }
       
       public function update($table, $what, $toWhat, $where) {
           $sql = 'UPDATE '.$table.' SET '.$what."='".$toWhat."' WHERE ".$where;

           echo($sql);

           return $this->_db->query($sql);
       }
       
       public function alreadyExist($table, $param, $condition) {
           return $this->select($table, $param, $condition) != null;
       }
       
       public function jobDone() {
           $this->_db->close();
       }
   }
?>