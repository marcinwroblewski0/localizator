<?php
    if(!isset($_SESSION)) session_start();
    session_regenerate_id();
    if(!$_SESSION["loggedIn"]) {
        header("Location: loginPage.html");
        exit();
    }
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="css/materialize.min.css" rel="stylesheet">
        <link href="css/font-awesome.min.css" rel="stylesheet">
        <link href="css/style.css" rel="stylesheet">

        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <title>Dodawanie pojazdu</title>

        <script src="jquery-3.0.0.min.js"
        type="text/javascript"></script>
         <script src="materialize.min.js"
        type="text/javascript"></script>
        <script src="smooth-scroll.js"
        type="text/javascript"></script>

        <style>

            .container {
                display: none;
            }

            ul.dropdown-content.select-dropdown li span {
                color: #333; /* no need for !important :) */
            }
        </style>
    </head>
    <body class="grey lighten-2">



        <nav>
            <div class="nav-wrapper grey darken-4">
            <a href="#" class="brand-logo dropdown-button" data-activates='logoutDropdown'>
                <ul id='logoutDropdown' class='dropdown-content'>
                    <li onclick="logout();"><img class="menu-image" src="images/logout.png"/>Wyloguj</li>
                </ul>
                <img class="profile-pic" src="images/hello.png"/>
                <?php

                    echo($_SESSION["name"]);
                ?>
            </a>
            <ul id="nav-mobile" class="right hide-on-med-and-down">
                <li><a href="account_creator.php"><img class="menu-image" src="images/add_user.png"/>Stwórz konto</a></li>
                <li><a href="car_configurator.php"><img class="menu-image" src="images/add.png"/>Dodaj pojazd</a></li>
                <li><a href="index.php"><img class="menu-image" src="images/control.png"/>Zarządaj</a></li>
            </ul>
            </div>
        </nav>

        <div class="container">
            <h1>Stwórz konto</h1>


            <div class="row">
                <div class="col s12">
                    <div class="input-field col s12 m6 l6">
                        <input placeholder="Login" id="login" type="text" name="login" class="validate">
                    </div>
                    <div class="input-field col s12 m6 l6">
                        <input placeholder="Hasło" id="password" name="password" type="password" class="validate">
                    </div>
                    <div class="input-field col s8">
                        <input placeholder="Imię i nazwisko" id="name" name="name" type="text" class="validate">
                    </div>
                    <div class="input-field col s4">
                        <select id="role">
                            <option value="2">Kierowca</option>
                            <option value="3">Nadzorujący</option>
                            <option value="1">Administrator</option>
                        </select>
                         <script>

                            $(document).ready(function() {
                                $('select').material_select();
                            });

                        </script>
                    </div>

                    <button class="btn waves-effect waves-light right grey darken-3" name="action" onclick="addAccount();">
                        <img class="small-icon" src="images/add.png"/>
                        Dodaj

                    </button>
                </div>
            </div>


        </div>
        </div>

        <div class="fixed-action-btn" style="bottom: 24px; right: 24px;">
            <!-- <a class="btn-floating btn-large blue-grey" href="car_configurator.php">
                <img class="fab-image" src="images/add.png"/>
            </a> -->
            <a class="btn-floating btn-large black click-to-toggle">
              <i class="material-icons">menu</i>
            </a>
            <ul>
              <li><a class="btn-floating grey darken-2" href="car_configurator.php"><i class="material-icons">directions_car</i></a></li>
              <li><a class="btn-floating grey darken-3" href="account_creator.php"><i class="material-icons">person_add</i></a></li>
              <li><a class="btn-floating grey darken-4" href="index.php"><i class="material-icons">view_carousel</i></a></li>
            </ul>
          </div>
        </div>

        <script src="logout.js"></script>

        <script>

            $( document ).ready(function() {
                $(".container").fadeIn();
            });


            function addAccount() {
                 console.log("Dodaję konto");

                var login = $("#login").val();
                var password = $("#password").val();
                var name = $("#name").val();
                var role = $("#role").val();

                $.post("add_account.php", {"login": login, "password": password, "name": name, "role": role}, function(data) {

                    console.log(data);
                    var response = JSON.parse(data);

                    if(response["result"] == "success") {
                        $(".container").append('<div class="row">' +
                            '<div class="col s12">' +
                                '<div class="card light-green darken-1">' +
                                    '<div class="card-content white-text">' +
                                    '<span class="card-title">' + response["info"] +
                                    '</div>' +
                                '</div>' +
                            '</div>' +
                        '</div>');
                    } else {
                        $(".container").append('<div class="row">' +
                            '<div class="col s12">' +
                                '<div class="card red darken-1">' +
                                    '<div class="card-content white-text">' +
                                    '<span class="card-title">' + response["info"] +
                                    '</div>' +
                                '</div>' +
                            '</div>' +
                        '</div>');
                    }
                });
            }
        </script>
    </body>
</html>
