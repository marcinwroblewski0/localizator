function convertUserAddressToGMAddressAsync(userAddress){
  return new Promise(function(resolve, reject) {
    var req = new XMLHttpRequest();
    req.open('GET', "https://maps.googleapis.com/maps/api/geocode/json?address=" + userAddress + "&key=AIzaSyDpJnROqRSjHR-cbJtDokfQIllmTLszfPU", false);
    req.send(null);


      var responseStatus = JSON.parse(req.responseText)["status"];
      console.log("Response status: " + responseStatus);
      if(responseStatus == "OK") {
        var resultsJSON = JSON.parse(req.responseText)["results"];
        var returnArray = new Array();

        for(var i = 0; i < resultsJSON.length; i++) {
          returnArray.push(resultsJSON[i]["formatted_address"]);
        }
        resolve(returnArray);
      } else if(responseStatus == "ZERO_RESULTS") {
        reject("Nie znaleziono takiego adresu");
      } else if(responseStatus == "OVER_QUERY_LIMIT") {
        reject("Przekroczono liczbę zapytań do serwisu map. Skontaktuj się z administratorem. Dalsza praca aktualnie nie jest możliwa.");
      } else if(responseStatus == "REQUEST_DENIED") {
        reject("Zapytanie zostało odrzucone. Spróbuj ponownie.");
      } else if(responseStatus == "INVALID_REQUEST") {
        reject("Błąd w zapytaniu. Skontaktuj się z administratorem.");
      } else if(responseStatus == "UNKNOWN_ERROR") {
        reject("Nieznany błąd. Spróbuj ponownie.");
      }


  });
}
