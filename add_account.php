<?php

    if(isset($_POST["sid"])) {
        session_id($_POST["sid"]);
    }
    if(!isset($_SESSION)) session_start();

    if(!isset($_SESSION["loggedIn"]) || !$_SESSION["loggedIn"]) {
        http_response_code(401);
        echo(json_encode(array('error' => "Not logged in")));
        exit();
    }

    require_once "SDM.php";
    require_once "db.php";

    if(!isset($_POST["login"]) || !isset($_POST["password"]) || !isset($_POST["name"]) || !isset($_POST["role"])) {
        http_response_code(200);
        echo(json_encode($_POST));
        echo(json_encode(array('error' => "Wrong data")));
        exit();
    }

    

    $sdm = new SDM($db_address, $db_user, $db_password, $db_name);

    if(!$sdm->alreadyExist('users', 'id', 'login="'.$_POST["login"].'"')) {
        $tableRow = array(
            'id' => 'AUTO_INCREMENT', 
            'login' => $_POST["login"],
            'password' => password_hash($_POST["password"], PASSWORD_DEFAULT),
            'name' => $_POST["name"],
            'role' => $_POST["role"]);
        $sdm->insert('users', $tableRow);
       

        echo(json_encode(array('result' => "success", 'info' => 'Stworzono konto <b>'.$_POST["login"].'</b>')));
    } else {
        echo(json_encode(array('result' => "error", 'info' => 'Konto <b>'.$_POST["login"].'</b> już istnieje!')));
    }

    $sdm->jobDone();
?>