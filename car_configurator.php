<?php
    if(!isset($_SESSION)) session_start();
    session_regenerate_id();
    if(!$_SESSION["loggedIn"]) {
        header("Location: loginPage.html");
        exit();
    }
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="css/materialize.min.css" rel="stylesheet">
        <link href="css/font-awesome.min.css" rel="stylesheet">
        <link href="css/style.css" rel="stylesheet">

        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <title>Dodawanie pojazdu</title>

        <script src="jquery-3.0.0.min.js"
        type="text/javascript"></script>
         <script src="materialize.min.js"
        type="text/javascript"></script>
        <script src="smooth-scroll.js"
        type="text/javascript"></script>

        <style>

            .container {
                display: none;
            }

        </style>
    </head>
    <body class="grey lighten-2">



        <nav>
            <div class="nav-wrapper grey darken-4">
            <a href="#" class="brand-logo dropdown-button" data-activates='logoutDropdown'>
                <ul id='logoutDropdown' class='dropdown-content'>
                    <li onclick="logout();"><img class="menu-image" src="images/logout.png"/>Wyloguj</li>
                </ul>
                <img class="profile-pic" src="images/hello.png"/>
                <?php

                    echo($_SESSION["name"]);
                ?><div class="fixed-action-btn" style="bottom: 24px; right: 24px;">
            <a class="btn-floating btn-large blue-grey" href="index.php">
                <img class="fab-image" src="images/control.png"/>
            </a>
        </div>
        <div class="fixed-action-btn" style="bottom: 24px; right: 24px;">
            <a class="btn-floating btn-large blue-grey" href="index.php">
                <img class="fab-image" src="images/control.png"/>
            </a>
        </div>
            </a>
            <ul id="nav-mobile" class="right hide-on-med-and-down">
                <li><a href="account_creator.php"><img class="menu-image" src="images/add_user.png"/>Stwórz konto</a></li>
                <li><a href="car_configurator.php"><img class="menu-image" src="images/add.png"/>Dodaj pojazd</a></li>
                <li><a href="index.php"><img class="menu-image" src="images/control.png"/>Zarządaj</a></li>
            </ul>
            </div>
        </nav>

        <div class="container">
            <h1>Dodaj pojazd</h1>


            <?php
            if(isset($_SESSION["newest_car"])) {
                echo('<div class="row">
                <div class="col s12">
                    <div class="card light-green darken-1">
                        <div class="card-content white-text">
                        <span class="card-title">Poprawnie dodano <b>'.$_SESSION["newest_car"].'</b></span>
                        <p>Pojazd został poprawnie dodany do bazy danych. Od teraz będzie dostępny w panelu zarządzania.</p>
                        </div>
                    </div>
                </div>
                </div>');
                unset($_SESSION["newest_car"]);
            } else if (isset($_SESSION["add_car_error"])) {
                echo('<div class="row">
                <div class="col s12">
                    <div class="card red darken-1">
                        <div class="card-content white-text">
                        <span class="card-title">Wystąpił <b>błąd</b></b></span>
                        <p>'.$_SESSION["add_car_error"].'</p>
                        </div>
                    </div>
                </div><div class="fixed-action-btn" style="bottom: 24px; right: 24px;">
            <a class="btn-floating btn-large blue-grey" href="index.php">
                <img class="fab-image" src="images/control.png"/>
            </a>
        </div>
                </div>');
                unset($_SESSION["add_car_error"]);
            }


            ?>

            <div class="row">
                <form class="col s12" action="add_car.php" method="POST">
                    <div class="input-field col s8">
                        <input placeholder="Nazwa pojazdu" id="car_name" type="text" name="car_name" class="validate">
                    </div>
                    <div class="input-field col s4">
                        <input placeholder="Pojemność maksymalna (kg)" id="max_capacity" name="max_capacity" type="number" class="validate">
                    </div>

                    <button class="btn waves-effect waves-light right  grey darken-3" type="submit" name="action">
                        <img class="small-icon" src="images/add.png"/>
                        Dodaj

                    </button>
                </form>
            </div>


        </div>
        </div>

        <div class="fixed-action-btn" style="bottom: 24px; right: 24px;">
            <!-- <a class="btn-floating btn-large blue-grey" href="car_configurator.php">
                <img class="fab-image" src="images/add.png"/>
            </a> -->
            <a class="btn-floating btn-large black click-to-toggle">
              <i class="material-icons">menu</i>
            </a>
            <ul>
              <li><a class="btn-floating grey darken-2" href="car_configurator.php"><i class="material-icons">directions_car</i></a></li>
              <li><a class="btn-floating grey darken-3" href="account_creator.php"><i class="material-icons">person_add</i></a></li>
              <li><a class="btn-floating grey darken-4" href="index.php"><i class="material-icons">view_carousel</i></a></li>
            </ul>
          </div>
        </div>

        <script src="logout.js"></script>

        <script>

            $( document ).ready(function() {
                $(".container").fadeIn();
            });

        </script>
    </body>
</html>
