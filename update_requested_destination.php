<?php

    
    
    
    if(isset($_POST["sid"])) {
        session_id($_POST["sid"]);
    }
    if(!isset($_SESSION)) session_start();

    if(!isset($_SESSION["loggedIn"]) || !$_SESSION["loggedIn"]) {
        http_response_code(200);
        echo(json_encode(array('error' => "Not logged in")));
        exit();
    }

    require_once "SDM.php";
    require_once "db.php";

    if(isset($_POST['requested_destination'])) {

        $sdm = new SDM($db_address, $db_user, $db_password, $db_name);   

        echo $sdm->update("cars", "requested_destination", $_POST['requested_destination'], "name='".$_POST["car_name"]."'");
        $sdm->jobDone();

        echo("ok");
        exit();
    }

?>

<form method="post">

    <input type="text" name="car_name"/>
    <input type="text" name="requested_destination"/>

    <input type="submit"/>
</form>