<?php

    if(!isset($_SESSION)) session_start();
    session_regenerate_id();
    if(isset($_POST["sid"])) {
        session_id($_POST["sid"]);
    }

    if(!isset($_SESSION["loggedIn"]) || !$_SESSION["loggedIn"]) {
        http_response_code(401);
        echo(json_encode(array('error' => "Not logged in")));
        exit();
    }

    foreach ($_SERVER as $key => $value) {
        echo($key." : ".$value."<br />");
    }

?>