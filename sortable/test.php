<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Ustal trasę</title>
  <link rel="stylesheet" href="jquery-ui.min.css">
  <link rel="stylesheet" href="materialize.min.css">
  
  <link href="../css/font-awesome.min.css" rel="stylesheet">
  <style>

    .ui-sortable-handle {
      cursor: move;
    }

    .remove-icon {
      cursor: pointer;
    }

    .ui-state-default {
      border: none;
    }

  </style>
  <script src="jquery-3.0.0.min.js"></script>
  <script src="jquery-ui.min.js"></script>
  <script src="materialize.min.js"></script>
  <script src="sortable.js"></script>
</head>
<body>
 <div class="container">
    <div class="row">
      <h4 id="title" class="col s12">Dodaj miejsce docelowe</h4>
      <input class="col s11" id="addPlaceInput" type="text" placeholder="Katowice, Polska"/>
      <a class="col btn-floating btn-large waves-effect waves-light blue right" style="font-size: 1.6em" onclick="addSortableOption($('#addPlaceInput').val())">+</a>
    </div>
    
     
      <h4 onclick="downloadDestinationsForSelectedCar()">Ustal trasę</h4>

    <ul id="sortable" class="collection z-depth-1">



    </ul>
 
    <button class="btn blue waves-effect waves-light right" onclick="sendDestinationsToDB(convertLocationSetToJSON(), this)">Gotowe
    </button>
 </div>

 <script>
 var selectedCar;

  function setSelectedCar(carName) {
    $("#title").html("Dodaj miejsce docelowe dla " + carName);
    selectedCar = carName;
    downloadDestinationsForSelectedCar();
  } 

  function sendDestinationsToDB(jsonText, button) {
    $.post("../update_destinations_list.php", {"car_name": selectedCar, "destinations": jsonText}, function(data) {
      console.log(data);

      var loadingAnimation = getLoadingAnimation();
      if(button != null) {
        button.parentNode.appendChild(loadingAnimation);
      }

      setTimeout(function() {
        $(loadingAnimation).empty();
        if(button != null) {
          button.innerHTML = "Wysłano " + "<i class='fa fa-check' aria-hidden='true'></i>" ;
        }
      }, 900);

    });
  } 

  function getLoadingAnimation() {

    var wrapper = document.createElement("div");
    wrapper.setAttribute("class", "preloader-wrapper small active right");
    wrapper.setAttribute("style", "margin-right: 10px;");

    var layer = document.createElement("div");
    layer.setAttribute("class", "spinner-layer spinner-blue-only");



    var circleLeft = document.createElement("div");
    circleLeft.setAttribute("class", "circle");

    var clipperLeft = document.createElement("div");
    clipperLeft.setAttribute("class", "circle-clipper left");

    clipperLeft.appendChild(circleLeft);



    var circleGap = document.createElement("div");
    circleGap.setAttribute("class", "circle");

    var gap = document.createElement("div");
    gap.setAttribute("class", "gap-patch");

    gap.appendChild(circleGap);




    var circleRight = document.createElement("div");
    circleRight.setAttribute("class", "circle");

    var clipperRight = document.createElement("div");
    clipperRight.setAttribute("class", "circle-clipper right");

    clipperRight.appendChild(circleRight);




    layer.appendChild(clipperLeft);
    layer.appendChild(gap);
    layer.appendChild(clipperRight);

    wrapper.appendChild(layer);

    return wrapper;

  //   <div class="preloader-wrapper small active">
  //    <div class="spinner-layer spinner-green-only">
  //     <div class="circle-clipper left">
  //       <div class="circle"></div>
  //     </div><div class="gap-patch">
  //       <div class="circle"></div>
  //     </div><div class="circle-clipper right">
  //       <div class="circle"></div>
  //     </div>
  //   </div>
  // </div>

  }

  function downloadDestinationsForSelectedCar() {


    $.post("../get_destinations_for_car.php", {"car_name": selectedCar}, function(data) {
      console.log(data);

      $("#sortable").empty();

      try {
        var json = JSON.parse(data);
        convertJSONToLocationSet(json["destinations"]);
      } catch(e) {
        console.log(e);
        $("#sortable").html(
        "<div class='card-panel red'>" + 
          "<span class='white-text'><h2 class='header'><i class='fa fa-frown-o fa-1x' aria-hidden='true'></i> Brak ustalonej trasy</h2>" +
          "Dodaj punkty docelowe i ułóż je w odpowiedniej kolejności." +
          "</span>" +
        "</div>");
      }


    });
  }

 </script>

</body>
</html>