$(function() {
    $( "#sortable" ).sortable();
});


function addSortableOption(location, untrusted) {
    if(location == null || location == "") return;


    var sortableList = document.getElementById("sortable");

    var listItem = document.createElement("li");
    listItem.setAttribute("class", "ui-state-default collection-item");

    var deleteIcon = document.createElement("a");
    deleteIcon.setAttribute("class", "remove-icon fa fa-times fa right");
    deleteIcon.setAttribute("aria-hidden", "true");
    deleteIcon.setAttribute("style", "margin-top: 5px; color: #e53935;");
    deleteIcon.setAttribute("onclick", "$(this).parent().hide(300, function(obj) {$(this).remove()});");

    if(untrusted)
      convertUserAddressToGMAddressAsync(location)
      .then(function(value) {
         // resolved
         location = value[0];

         listItem.setAttribute("data-location", location);
         listItem.innerHTML = location;
         listItem.appendChild(deleteIcon);
         sortableList.appendChild(listItem);

         $("#sortable").sortable();
        }, function(reason) {
        // rejection
          document.getElementById("addPlaceInput").style.color = "#e53935";
          document.getElementById("addPlaceInput").value = reason;

      });
      else {
         listItem.innerHTML = location;
         listItem.appendChild(deleteIcon);
         sortableList.appendChild(listItem);
      }


}


function convertLocationSetToJSON() {
    var locationSet = document.getElementById("sortable");
    var locationArray = [];

    for(var i = 0; i < locationSet.childElementCount; i++) {
        var destination = locationSet.children[i].getAttribute("data-location");
        if(destination != null && destination != "") {
            locationArray.push(destination);
        }
    }
    return JSON.stringify(locationArray);
}

function convertJSONToLocationSet(jsonString) {
    var json = JSON.parse(jsonString);

    for(var i = 0; i < json.length; i++) {
        addSortableOption(json[i]);
    }
}

//TODO weryfikacja adresu
