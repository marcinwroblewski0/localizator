<?php
     
    if(isset($_POST["sid"])) {
        session_id($_POST["sid"]);
    }
    if(!isset($_SESSION)) session_start();

    if(!isset($_SESSION["loggedIn"]) || !$_SESSION["loggedIn"]) {
        http_response_code(200);
        echo(json_encode(array('error' => "Not logged in")));
        
        exit();
    }

    if(!isset($_POST["car_name"])) {
        echo(array('error' => 'Bad data'));
        exit();
    }

    require_once "SDM.php";
    require_once "db.php";

    $sdm = new SDM($db_address, $db_user, $db_password, $db_name);
    $carInfo = $sdm->select("cars", "destinations", "name='".$_POST["car_name"]."'");

    $sdm->jobDone();
    echo(json_encode($carInfo[0]));
?>