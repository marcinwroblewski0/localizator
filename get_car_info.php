<?php
     
    if(isset($_POST["sid"])) {
        session_id($_POST["sid"]);
    }
    if(!isset($_SESSION)) session_start();

    if(!isset($_SESSION["loggedIn"]) || !$_SESSION["loggedIn"]) {
        http_response_code(200);
        echo(json_encode(array('error' => "Not logged in")));
        
        exit();
    }

    if(!isset($_POST["car_name"])) {
        echo(array('error' => 'Bad data'));
        exit();
    }


    require_once "SDM.php";
    require_once "car.php";
    require_once "location.php";
    require_once 'db.php';

    
    $sdm = new SDM($db_address, $db_user, $db_password, $db_name);
    $carInfo = $sdm->select("cars", "*", "name='".$_POST["car_name"]."'");

    $driverId = $carInfo[0]["driver"];
    $driverName;
    if($driverId != 0)
        $driverName = $sdm->select("users", "name", "id=".$driverId)[0]["name"];
    else 
        $driverName = "Brak";

    $stateId = $carInfo[0]["state"];
    $stateName = $sdm->select("states", "name", "id=".$stateId)[0]["name"];

    $carInfo[0]["driver"] = "(".$driverId.") ".$driverName;
    $carInfo[0]["state"] = $stateName;

    foreach ($carInfo[0] as $key => $value) {
        if(trim($value)==='') $carInfo[0][$key] = "Brak";
    }

    $sdm->jobDone();
    echo(json_encode($carInfo));
?>