<?php
    if(!isset($_SESSION)) session_start();
    session_regenerate_id();

    if(!$_SESSION["loggedIn"]) {
        header("Location: loginPage.html");
        exit();
    }

?>

<!DOCTYPE html>

<html>
    <head>
        <meta charset="utf-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="css/materialize.min.css" rel="stylesheet">
        <link href="css/font-awesome.min.css" rel="stylesheet">
        <link href="css/style.css" rel="stylesheet">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <title>Panel zarządzania</title>
        <script src="jquery-3.0.0.min.js"
        type="text/javascript"></script>
        <script src="materialize.min.js"
        type="text/javascript"></script>
        <script src="smooth-scroll.js"
        type="text/javascript"></script>
        <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
        <script src="maps.js"></script>
        <script src="sortable/sortable.js"
        type="text/javascript"></script>
        <script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyDpJnROqRSjHR-cbJtDokfQIllmTLszfPU"></script>


        <style>

            .container {
                display: none;
            }

            #carDetailsModal .modal-content .collection {
                overflow: scroll;
            }

            #destinationModal {
                overflow-y: auto;
                padding-bottom: 1em;
            }

            .ui-sortable-handle, .ui-state-default {
                cursor: move;
            }

            .remove-icon {
                cursor: pointer;
            }

            .ui-state-default {
                border: none;
            }

            #sortable {
                height: auto;
                margin-bottom: 8px;
            }

            ul.dropdown-content.select-dropdown li span {
                color: #333; /* no need for !important :) */
            }
        </style>

        <script>

        var mapProp, map;
        var markersArray = new Array();
        var carsArray = new Array();
        var currentSortOption = 0;
        var directionsDisplay;
        var directionsService = new google.maps.DirectionsService();
        var base = new google.maps.LatLng(50.270908, 19.039993);

        function initialize() {
            directionsDisplay = new google.maps.DirectionsRenderer();
            mapProp = {
                center: base,
                zoom:15,
                mapTypeId:google.maps.MapTypeId.ROADMAP
            };
            map = new google.maps.Map(document.getElementById("googleMap"),mapProp);
            directionsDisplay.setMap(map);
        }

        //TODO create markers

        function setMarkerFor(name) {
            var lng, lat;

            for(var i = 0; i < markersArray.length; i++) {
                if(markersArray[i][2] == name) {
                    lng = markersArray[i][0];
                    lat = markersArray[i][1];
                }
            }

            var request = {
                origin: base,
                destination: new google.maps.LatLng(lat, lng),
                travelMode: google.maps.TravelMode.DRIVING
            };

            directionsService.route(request, function(response, status) {
                if (status == google.maps.DirectionsStatus.OK) {
                    directionsDisplay.setDirections(response);
                    $("#distance" + name).html(response.routes[0].legs[0].distance.text);
                }
            });

            map.setCenter(new google.maps.LatLng(lat, lng));
            console.log("LatLng " + lat + lng);
        }

        function createMapMarkerFor(id, longtitude, langtitude, name) {
            markersArray.push([longtitude, langtitude, name]);
        }

        //debug
        function printObject(o) {
            var out = '';
            for (var p in o) {
                out += p + ': ' + o[p] + '\n';
            }
            console.log(out);
        }

        function displayCarsArray() {
            $("#mainContainer").fadeOut(300, function() {
                $("#mainContainer").empty();
                for(var i = 0; i < carsArray.length; i++) {
                    createCarInfoPanel(i, carsArray[i]['name'], carsArray[i]['current_capacity'] + " / " + carsArray[i]['max_capacity'], carsArray[i]['gauge'], carsArray[i]['longtitude'], carsArray[i]['state']);
                }
                $("#mainContainer").fadeIn(300);
            });
        }

        function createCarInfoPanel(id, name, capacity, gauge, distance, state) {
            var col = document.createElement("div");

            col.setAttribute("class", "col s12 m6 l4");


            var card = document.createElement("div");
            card.setAttribute("class", "card grey darken-4 z-depth-2");

            var cardContent = document.createElement("div");
            cardContent.setAttribute("class", "card-content white-text");

            var cardTitle = document.createElement("span");
            cardTitle.setAttribute("class", "card-title activator");
            cardTitle.innerHTML += name;

            var cardBadge = document.createElement("span");
            cardBadge.setAttribute("class", "badge white-text grey darken-3");
            cardBadge.setAttribute("style", "border-radius: 2px;")
            cardBadge.innerHTML = convertStateToText(state);

            cardTitle.innerHTML += convertCapacityToImage(parseInt(capacity.substring(capacity.length - 4)));
            cardTitle.appendChild(cardBadge);

//////////////////////////////////////////////////////////////////////////////////
            var capacityCollection = document.createElement("ul");
            capacityCollection.setAttribute("class", "collection");

            var capacityInfoItem = document.createElement("li");
            capacityInfoItem.setAttribute("class", "collection-item avatar grey darken-4");

            var capacityImage = document.createElement("img");
            capacityImage.setAttribute("class", "circle");
            capacityImage.setAttribute("src", "images/capacity.png");

            var capacityTitle = document.createElement("span");
            capacityTitle.setAttribute("class", "title");
            capacityTitle.innerHTML = "Ładowność";

            var capacityText = document.createElement("p");
            capacityText.innerHTML = capacity + " kg";

            //make capacity tree

            capacityInfoItem.appendChild(capacityImage);
            capacityInfoItem.appendChild(capacityTitle);
            capacityInfoItem.appendChild(capacityText);
            capacityCollection.appendChild(capacityInfoItem);

//////////////////////////////////////////////////////////////////////////////////
            var gaugeCollection = document.createElement("ul");
            gaugeCollection.setAttribute("class", "collection");

            var gaugeInfoItem = document.createElement("li");
            gaugeInfoItem.setAttribute("class", "collection-item avatar grey darken-4");

            var gaugeImage = document.createElement("img");
            gaugeImage.setAttribute("class", "circle");
            gaugeImage.setAttribute("src", "images/gauge.png");

            var gaugeTitle = document.createElement("span");
            gaugeTitle.setAttribute("class", "title");
            gaugeTitle.innerHTML = "Gabaryt";

            var gaugeText = document.createElement("p");
            gaugeText.innerHTML = gauge;
            //make gauge tree

            gaugeInfoItem.appendChild(gaugeImage);
            gaugeInfoItem.appendChild(gaugeTitle);
            gaugeInfoItem.appendChild(gaugeText);
            gaugeCollection.appendChild(gaugeInfoItem);
//////////////////////////////////////////////////////////////////////////////////
            var distanceCollection = document.createElement("ul");
            distanceCollection.setAttribute("class", "collection");

            var distanceInfoItem = document.createElement("li");
            distanceInfoItem.setAttribute("class", "collection-item avatar grey darken-4");

            var distanceImage = document.createElement("img");
            distanceImage.setAttribute("class", "circle");
            distanceImage.setAttribute("src", "images/distance.png");

            var distanceTitle = document.createElement("span");
            distanceTitle.setAttribute("class", "title");
            distanceTitle.innerHTML = "Odległość";

            var distanceText = document.createElement("p");
            distanceText.innerHTML = distance + " km";
            distanceText.setAttribute("id", 'distance' + name);

            var service = new google.maps.DistanceMatrixService();
                service.getDistanceMatrix(
                {
                    origins: [base],
                    destinations: [new google.maps.LatLng(carsArray[id]["langtitude"], carsArray[id]["longtitude"])],
                    travelMode: google.maps.TravelMode.DRIVING
                }, function (response, status) {
                    if(status == google.maps.DistanceMatrixStatus.OK) {
                        distanceText.innerHTML = response.rows[0].elements[0].distance.text + " (" + response.rows[0].elements[0].duration.text + ")";
                        carsArray[id]["distance"] = response.rows[0].elements[0].distance.value;
                    } else {
                        carsArray[id]["distance"] = 0;
                    }
                });

            //make distance tree
            distanceInfoItem.appendChild(distanceImage);
            distanceInfoItem.appendChild(distanceTitle);
            distanceInfoItem.appendChild(distanceText);
            distanceCollection.appendChild(distanceInfoItem);




            var showMore = document.createElement("span");
            showMore.setAttribute("style", "cursor: pointer; display: inline-block; margin-left: 0.6em; margin-bottom: 0.4em; margin-top: 0.4em;");
            showMore.setAttribute("onclick", "showCarDetailsModal('" + name + "')");

            var moreIcon = document.createElement("img");
            moreIcon.setAttribute("src", "images/more.png");
            moreIcon.setAttribute("class", "profile-pic");
            moreIcon.setAttribute("style", "display: inline-block;");

            var moreDescription = document.createElement("p");
            moreDescription.innerHTML = "WIĘCEJ";
            moreDescription.setAttribute("style", "display: inline-block; margin-left: 0.4em;");

            showMore.appendChild(moreIcon);
            showMore.appendChild(moreDescription);



            var cardAction = document.createElement("div");
            cardAction.setAttribute("class", "card-action");

            var showLocationButton = document.createElement("a");
            showLocationButton.innerHTML = "lokalizacja";
            showLocationButton.setAttribute("onclick", 'setMarkerFor("' + name + '")');
            showLocationButton.setAttribute("href", "#googleMap");
            showLocationButton.setAttribute("data-scroll", "");

            var requestDestinationButton = document.createElement("a");
            requestDestinationButton.innerHTML = "trasa";
            requestDestinationButton.setAttribute("onclick", 'showDestinationModal("' + name + '")');
            requestDestinationButton.setAttribute("href", "#destinationModal");
            requestDestinationButton.setAttribute("class", "modal-trigger");

            cardAction.appendChild(showLocationButton);
            cardAction.appendChild(requestDestinationButton);

            cardContent.appendChild(cardTitle);
            cardContent.appendChild(capacityCollection);
            cardContent.appendChild(gaugeCollection);
            cardContent.appendChild(distanceCollection);
            cardContent.appendChild(showMore);
            cardContent.appendChild(cardAction);

            var cardReveal = document.createElement("div");
            cardReveal.setAttribute("class", "card-reveal");

            var cardRevealTitle = document.createElement("span");
            cardRevealTitle.setAttribute("class", "card-title red-text text-darken-1");
            cardRevealTitle.innerHTML = "Usuń pojazd";

            var cardRevealContent = document.createElement("p");
            cardRevealContent.setAttribute("class", "card-title grey-text text-darken-4");
            cardRevealContent.innerHTML = "Czy na pewno chcesz usunąć pojazd?";

            var cardRevealButton = document.createElement("button");
            cardRevealButton.setAttribute("class", "waves-effect waves-light btn red white-text right");
            cardRevealButton.setAttribute("onclick", "sendDeleteCarRequest('" + name + "'); $(this.parentNode.parentNode).hide(300);");
            cardRevealButton.innerHTML = "Usuń";

            cardReveal.appendChild(cardRevealTitle);
            cardReveal.appendChild(cardRevealContent);
            cardReveal.appendChild(cardRevealButton);

            //reveal driver info
            var cardRevealDriverInfo = document.createElement("ul");
            cardRevealDriverInfo.setAttribute("class", "collection");

            var cardRevealDriverItem = document.createElement("li");
            cardRevealDriverItem.setAttribute("class", "collection-item avatar white darken-1");

            var cardRevealDriverImage = document.createElement("img");
            cardRevealDriverImage.setAttribute("class", "circle");
            cardRevealDriverImage.setAttribute("src", "images/distance.png");

            var cardRevealDriverTitle = document.createElement("span");
            cardRevealDriverTitle.setAttribute("class", "title");
            cardRevealDriverTitle.innerHTML = "Kierowca";

            var cardRevealDriverText = document.createElement("p");
            cardRevealDriverText.innerHTML = distance + " km";
            cardRevealDriverText.setAttribute("id", 'distance' + name);


            cardRevealDriverItem.appendChild(cardRevealDriverImage);
            cardRevealDriverItem.appendChild(cardRevealDriverTitle);
            cardRevealDriverItem.appendChild(cardRevealDriverText);
            cardRevealDriverInfo.appendChild(cardRevealDriverItem);
            //////////////////////////////////////////////////////////////////////////////////////////////



            card.appendChild(cardContent);
            card.appendChild(cardReveal);
            col.appendChild(card);

            document.getElementById("mainContainer").insertBefore(
                col, document.getElementById("mainContainer").firstChild);

        }


        function sendDeleteCarRequest(name) {
            $.ajax({
                url: "delete_car.php",   // URL of your php script
                type: "post",
                data: {'name': name},
                success: function(data) {
                    console.log(data);
                }
            });
        }

        function convertCapacityToImage(capacity) {
            if(capacity < 500) {
                return "<img class='vehicle-type-image' src='images/car.png'/>";
            } else if(capacity > 500 && capacity < 1000) {
                return "<img class='vehicle-type-image' src='images/truck1.png'/>";
            } else if(capacity > 1000) {
                return "<img class='vehicle-type-image' src='images/truck2.png'/>";
            }

            return ".";
        }

        function convertStateToText(state) {
            switch (parseInt(state)) {
                case 0:
                    return "Wolny";
                    break;
                case 1:
                    return "W drodze";
                    break;
                case 2:
                    return "Rozładunek/Załadunek";
                    break;
                case 3:
                    return "W drodze do bazy";
                    break;
                default:
                    return "?";
                    break;
            }
        }

        function compareCapacity(a,b) {
            return a['current_capacity'] - b['current_capacity'];
        }


        function compareDistance(a,b) {
            return a['distance'] - b['distance'];
        }

        function compareState(a,b) {
            return parseInt(b['state']) - parseInt(a['state']);
        }

        function sortCarsBy(option) {
            currentSortOption = option;
            switch (parseInt(currentSortOption) - 1) {
                case 0:
                    console.log('by capacity');
                    carsArray = carsArray.sort(compareCapacity);
                    break;
                case 1:
                    console.log('by distance');
                    carsArray = carsArray.sort(compareDistance);
                    break;
                case 2:
                    console.log('by state');
                    carsArray = carsArray.sort(compareState);
                    break;
                default:
                    console.log('by default');
                    carsArray = carsArray.sort().reverse();
                    break;
            }
            displayCarsArray();
        }

        function gatherAndDisplayAllCarsInfo() {

            $.post("get_all_cars.php", function(data) {
                console.log(data);
                carsArray = JSON.parse(data);

                for(var i = 0; i < carsArray.length; i++) {
                    carsArray[i] = $.parseJSON(carsArray[i]);
                    createMapMarkerFor(parseInt(carsArray[i]["id"]), carsArray[i]["longtitude"], carsArray[i]["langtitude"], carsArray[i]["name"]);
                }
                sortCarsBy(currentSortOption);
                console.log(carsArray);
            });

            setTimeout(gatherAndDisplayAllCarsInfo, 300000);

        }
        google.maps.event.addDomListener(window, 'load', initialize);
        </script>

    </head>

    <body class="grey lighten-2">

            <nav>
                <div class="nav-wrapper blue-grey darken-4">
                <a href="#" class="brand-logo dropdown-button" data-activates='logoutDropdown'>
                    <ul id='logoutDropdown' class='dropdown-content'>
                        <li onclick="logout();"><img class="menu-image" src="images/logout.png"/>Wyloguj</li>
                    </ul>
                    <img class="profile-pic" src="images/hello.png"/>
                    <?php

                        echo("Witaj ".$_SESSION["name"]);
                    ?>
                </a>
                <ul id="nav-mobile" class="right hide-on-med-and-down">
                  <li><a href="account_creator.php"><img class="menu-image" src="images/add_user.png"/>Stwórz konto</a></li>
                  <li><a href="car_configurator.php"><img class="menu-image" src="images/add.png"/>Dodaj pojazd</a></li>
                  <li><a href="index.php"><img class="menu-image" src="images/control.png"/>Zarządaj</a></li>
                </ul>
                </div>
            </nav>

        <div class="container">


            <div class="row input-field col s12">
                <select id="sortOptions" onchange="sortCarsBy($(this).val());">
                <option value="" selected>Alfabetycznie</option>
                <option value="1">Dostępna ładowność</option>
                <option value="2">Odległość</option>
                <option value="3">Stan</option>
                </select>
            </div>

            <script>

                $(document).ready(function() {
                    $('select').material_select();
                });

            </script>


            <div id="mainContainer" class="row">

                <script>

                    gatherAndDisplayAllCarsInfo();

                </script>

            </div>

        <div class="row">
            <div id="googleMap" class="z-depth-1 col s12"></div>
        </div>
        <div class="fixed-action-btn" style="bottom: 24px; right: 24px;">
            <!-- <a class="btn-floating btn-large blue-grey" href="car_configurator.php">
                <img class="fab-image" src="images/add.png"/>
            </a> -->
            <a class="btn-floating btn-large black click-to-toggle">
              <i class="material-icons">menu</i>
            </a>
            <ul>
              <li><a class="btn-floating grey darken-2" href="car_configurator.php"><i class="material-icons">directions_car</i></a></li>
              <li><a class="btn-floating grey darken-3" href="account_creator.php"><i class="material-icons">person_add</i></a></li>
              <li><a class="btn-floating grey darken-4" href="index.php"><i class="material-icons">view_carousel</i></a></li>
            </ul>
          </div>
        </div>

        <!-- Request new direction modal -->
        <div id="destinationModal" class="modal">
            <div class="modal-content">
                <div class="row">
                    <h4 id="destinationModalTitle" class="col s12">Dodaj miejsce docelowe</h4>
                    <input class="col s11" id="addPlaceInput" type="text" placeholder="Katowice, Polska"/>
                    <a class="col btn-floating btn-large waves-effect waves-light grey darken-4 right" style="font-size: 1.6em" onclick="addSortableOption($('#addPlaceInput').val(), true)">+</a>
                    </div>

                    <h4 onclick="downloadDestinationsForSelectedCar()">Ustal trasę</h4>

                    <ul id="sortable" class="collection z-depth-1">
                    </ul>
                    <button class="btn-flat waves-effect waves-light right" onclick="sendDestinationsToDB(convertLocationSetToJSON(), this)">Gotowe</button>
            </div>

        </div>

        <script>


            var requestDestinationCarName;

            function showDestinationModal(carName) {
                document.getElementById("destinationModalTitle").innerHTML = "Dodaj miejsce docelowe dla pojazdu <b>" + carName + "</b>";
                $("#destinationModal").openModal();
                requestDestinationCarName = carName;
                setSelectedCar(requestDestinationCarName);
            }


            function setSelectedCar(carName) {
                $("#title").html("Dodaj miejsce docelowe dla " + carName);
                downloadDestinationsForSelectedCar();
            }

            function sendDestinationsToDB(jsonText, button) {



                $.post("update_destinations_list.php", {"car_name": requestDestinationCarName, "destinations": jsonText}, function(data) {
                console.log(data);

                var loadingAnimation = getLoadingAnimation();
                if(button != null) {
                    button.parentNode.appendChild(loadingAnimation);
                }

                setTimeout(function() {
                    $(loadingAnimation).empty();
                    if(button != null) {
                    button.innerHTML = "Wysłano " + "<i class='fa fa-check' aria-hidden='true'></i>" ;

                    setTimeout(function() {
                        $("#destinationModal").closeModal();

                        button.innerHTML = "Gotowe";
                    }, 300);

                  }green
                }, 900);

                });
            }

            function getLoadingAnimation() {

                var wrapper = document.createElement("div");
                wrapper.setAttribute("class", "preloader-wrapper small active right");
                wrapper.setAttribute("style", "margin-right: 10px;");

                var layer = document.createElement("div");
                layer.setAttribute("class", "spinner-layer spinner-blue-only");



                var circleLeft = document.createElement("div");
                circleLeft.setAttribute("class", "circle");

                var clipperLeft = document.createElement("div");
                clipperLeft.setAttribute("class", "circle-clipper left");

                clipperLeft.appendChild(circleLeft);



                var circleGap = document.createElement("div");
                circleGap.setAttribute("class", "circle");

                var gap = document.createElement("div");
                gap.setAttribute("class", "gap-patch");

                gap.appendChild(circleGap);




                var circleRight = document.createElement("div");
                circleRight.setAttribute("class", "circle");

                var clipperRight = document.createElement("div");
                clipperRight.setAttribute("class", "circle-clipper right");

                clipperRight.appendChild(circleRight);




                layer.appendChild(clipperLeft);
                layer.appendChild(gap);
                layer.appendChild(clipperRight);

                wrapper.appendChild(layer);

                return wrapper;

            //   <div class="preloader-wrapper small active">
            //    <div class="spinner-layer spinner-green-only">
            //     <div class="circle-clipper left">
            //       <div class="circle"></div>
            //     </div><div class="gap-patch">
            //       <div class="circle"></div>
            //     </div><div class="circle-clipper right">
            //       <div class="circle"></div>
            //     </div>
            //   </div>
            // </div>

            }

            function downloadDestinationsForSelectedCar() {


                $.post("get_destinations_for_car.php", {"car_name": requestDestinationCarName}, function(data) {
                console.log(data);

                $("#sortable").empty();

                try {
                    var json = JSON.parse(data);
                    if(json["destinations"] == "" || json["destinations"] == null || json["destinations"] == "[null]" || json["destinations"] == "[]") {throw "No data in JSON"};

                    convertJSONToLocationSet(json["destinations"]);
                } catch(e) {
                    console.log(e);
                    $("#sortable").html(
                    "<div class='card-panel red' onclick='$(this).remove()'> <i class='fa fa-times fa-1x right white-text' style='cursor: pointer' aria-hidden='true'></i> " +
                    "<span class='white-text'><h2 class='header'><i class='fa fa-frown-o fa-1x' aria-hidden='true'></i> Brak ustalonej trasy</h2>" +
                    "Dodaj punkty docelowe i ułóż je w odpowiedniej kolejności." +
                    "</span>" +
                    "</div>");
                }


                });
            }

        </script>
        <!-- End of new direction modal -->



        <!-- Car details modal -->
        <div id="carDetailsModal" class="modal">
            <div class="modal-content">
                <h4 id="carDetailsModalTitle">Nazwa pojazdu</h4>
                <ul id="carDetailsModalCollection" class="collection" style="height: 390px;">

                </ul>
            </div>
            <div class="modal-footer">
                <a class="modal-action modal-close waves-effect waves-black btn-flat">Zamknij</a>
            </div>
        </div>

        <script>

            function showCarDetailsModal(carName) {
                document.getElementById("carDetailsModalTitle").innerHTML = "Szczegóły o <b>" + carName + "</b>";
                $("#carDetailsModal").openModal();

                $.post("get_car_info.php", {car_name : carName}, function(data) {
                    $("#carDetailsModalCollection").empty();
                    console.log(data);

                    var response = JSON.parse(data);

                    $("#carDetailsModalCollection").append(createCollectionItem("Identyfikator", response[0]["id"]));
                    $("#carDetailsModalCollection").append(createCollectionItem("Nazwa", response[0]["name"]));
                    $("#carDetailsModalCollection").append(createCollectionItem("Kierowca", response[0]["driver"]));
                    $("#carDetailsModalCollection").append(createCollectionItem("Maksymalna ładowność (kg)", response[0]["max_capacity"]));
                    $("#carDetailsModalCollection").append(createCollectionItem("Dostępna ładowność (kg)", response[0]["current_capacity"]));

                    var location = createCollectionItem("Lokalizacja", response[0]["name"]);
                    $("#carDetailsModalCollection").append(location);

                    $.get("https://maps.googleapis.com/maps/api/geocode/json?latlng=" + response[0]["langtitude"] + " ," + response[0]["longtitude"] + "&key=AIzaSyDpJnROqRSjHR-cbJtDokfQIllmTLszfPU", function(data) {
                        console.log(data);
                        if(data["status"] == "OK")
                            location.innerHTML = "<b>Lokalizacja:</b> " + data["results"][0]["formatted_address"];
                        else
                            location.innerHTML = "<b>Lokalizacja:</b> Nieznana";
                    });

                    $("#carDetailsModalCollection").append(createCollectionItem("Stan", response[0]["state"]));
                    $("#carDetailsModalCollection").append(createCollectionItem("Następny cel", response[0]["current_destination"]));
                    $("#carDetailsModalCollection").append(createCollectionItem("Sugerowany cel", response[0]["requested_destination"]));


                });
            }

            function createCollectionItem(title, value) {
                var item = document.createElement("li");
                item.setAttribute("class", "collection-item grey lighten-5");

                item.innerHTML = "<b>" + title + ":</b> " + value;

                return item;
            }

        </script>
        <!-- End of car details modal -->


        <script src="logout.js"></script>
        <script>
             smoothScroll.init();
            $( document ).ready(function() {
                $(".container").fadeIn();
            });

        </script>
    </body>
</html>
