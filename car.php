<?php

    require 'location.php';

    class Car {
        //variables for not active vehicle
        public $id, $name, $maxCapacity, $gauge;

        //variables for active vehicle
        public $location, $currentCapacity;


        function __construct ($name, $maxCapacity) {
            
            //TODO sprawdz czy samochod istnieje
            //jeżeli nie, stwórz wpis w bazie
            //w innym przypadku pobierz id z bazy
            $id = 10; 
            if(!$this->isRegistered($name)) {
                $this->location = new Location(0, 0);
                $this->currentCapacity = $maxCapacity;
            } else {
                //TODO pobieranie danych z bazy
                $this->location = new Location(31, 10);
                $this->currentCapacity = 756;
            }
            

            $this->name = $name;
            $this->maxCapacity = $maxCapacity;

            if($this->maxCapacity < 500) {
                $this->gauge = "Samochód dostawczy";
            } else if ($this->maxCapacity > 500 && $this->maxCapacity < 1000){
                $this->gauge = "Samochód ciężarowy";
            } else if ($this->maxCapacity > 1000) {
                $this->gauge = "Ciągnik z naczepą";
            }
        }

        function isRegistered($name) {
            return false;
        }

        function introduce() {
            return array(
                'id' => $this->id,
                'name' => $this->name,
                'maxCapacity' => $this->maxCapacity,
                'currentCapacity' => $this->currentCapacity,
                'gauge' => $this->gauge,
                'location' => $this->location->getCoordinates()
                );
        }

        function introduceInJSON() {
            return json_encode(
                array(
                'id' => $this->id,
                'name' => $this->name,
                'maxCapacity' => $this->maxCapacity,
                'currentCapacity' => $this->currentCapacity,
                'gauge' => $this->gauge,
                'location' => $this->location->getCoordinatesJSON()
            ));
        }
    }

?>