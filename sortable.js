$(function() {
    var sortableList = document.getElementById("sortable");

    for(var i = 0; i < 3; i++) {
        var listItem = document.createElement("li");
        listItem.setAttribute("class", "ui-state-default collection-item");
        listItem.setAttribute("data-location", "Katowice, Polska");
        
        var deleteIcon = document.createElement("span");
        deleteIcon.setAttribute("class", "ui-icon ui-icon-close right");
        deleteIcon.setAttribute("onclick", "$(this).parent().remove();");
        
        listItem.innerHTML = "Katowice, Polska";
        listItem.appendChild(deleteIcon);

        sortableList.appendChild(listItem);
    }

    $( "#sortable" ).sortable();
});


function addSortableOption(location) {
    if(location == null || location == "") return;

    var sortableList = document.getElementById("sortable");
    
    var listItem = document.createElement("li");
    listItem.setAttribute("class", "ui-state-default collection-item");
    listItem.setAttribute("data-location", location);

    var deleteIcon = document.createElement("span");
    deleteIcon.setAttribute("class", "ui-icon ui-icon-close right");
    deleteIcon.setAttribute("onclick", "$(this).parent().remove();");
    
    listItem.innerHTML = location;
    listItem.appendChild(deleteIcon);

    sortableList.appendChild(listItem);


    $("#sortable").sortable();
}


function convertLocationSetToJSON() {
    var locationSet = document.getElementById("sortable");
    var locationArray = [];

    for(var i = 0; i < locationSet.childElementCount; i++) {
        locationArray.push(locationSet.children[i].getAttribute("data-location"));
    }
    return JSON.stringify(locationArray);
}

function convertJSONToLocationSet(jsonString) {
    var json = JSON.parse(jsonString);

    for(var i = 0; i < json.length; i++) {
        addSortableOption(json[i]);
    }
}