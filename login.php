<?php

	require_once "SDM.php";
	

	class Login {

		function verify($login, $password) {
			
			require_once "db.php";

			if ((!isset($_POST['login'])) || (!isset($_POST['password']))) {
				echo(json_encode(array('error' => "Bad data")));
				http_response_code(200);
				exit();
			}

			$polaczenie = new mysqli($db_address, $db_user, $db_password, $db_name);

			if ($polaczenie->connect_errno) {
				echo "Error: ".$polaczenie->connect_errno;
				
			} else {
				$login = $_POST['login'];
				$haslo = $_POST['password'];
			
				if ($rezultat = $polaczenie->query(
				sprintf("SELECT * FROM users WHERE login='%s'",
				mysqli_real_escape_string($polaczenie, $login)))) {
					$wiersz = $rezultat->fetch_assoc();

					$ilu_userow = $rezultat->num_rows;
					if($ilu_userow>0) {
						
						if(password_verify($haslo, $wiersz["password"])){							

							$_SESSION['loggedIn'] = true;
							$_SESSION['id'] = $wiersz['id'];
							$_SESSION['name'] = $wiersz['name'];
							$_SESSION['login'] = $wiersz['login'];
							
							$sdm = new SDM($db_address, $db_user, $db_password, $db_name);
							$_SESSION['role'] = $sdm->select("roles", "name", "id='".$wiersz["role"]."'")[0]["name"];
							$sdm->jobDone();

							echo(json_encode(array('result' => 'success', 'role' => $_SESSION['role'])));
							if(isset($_POST["want_sid"])) echo("sid=".session_id());
							
							$rezultat->free_result();
						} else {
							echo(json_encode(array('result' => 'Bad password')));
							http_response_code(401);
						}
					} else {
						echo(json_encode(array('result' => 'User not found')));
						http_response_code(401);
					}
				} else {
					echo($rezultat." nie ok ".$polaczenie->connect_errno);
				}
				
				$polaczenie->close();
			}
		}

		function logout() {
			echo("User logout: ".$_SESSION["login"]);
			session_destroy();
		}

	}

	if(!isset($_SESSION)) session_start();
    session_regenerate_id();

	$login = new Login();

	if(isset($_POST["logout"])) {
		$login->logout();
		exit();
	}

	$login->verify($_POST["login"], $_POST["password"]);
	
	
?>