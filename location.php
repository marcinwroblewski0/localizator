<?php 

    class Location {
        public $longtitude, $langtitude;

        function __construct ($longtitude, $langtitude) {
            $this->longtitude = $longtitude;
            $this->langtitude = $langtitude;
        }

        function getCoordinatesArray() {
            return array('longtitude' => $this->longtitude, 'langtitude' => $this->langitude);
        }

        function getCoordinatesJSON() {
            return json_encode(
                $this->getLocation()
            );
        }

        function getLongtitude() {
            return $this->longtitude;
        }

        function getLangtitude() {
            return $this->langtitude;
        }
    }

?>