<?php

    require 'SDM.php';  
    require 'car.php';
    require 'db.php';

    if(isset($_POST["sid"])) {
        session_id($_POST["sid"]);
    }
    
    if(!isset($_SESSION)) session_start();
    session_regenerate_id();
    if(!isset($_SESSION["loggedIn"]) || !$_SESSION["loggedIn"]) {
        http_response_code(401);
        echo(json_encode(array('error' => "Not logged in")));
        exit();
    }

    $car;
    if(isset($_POST['car_name'])) {
        $car = new Car($_POST['car_name'], $_POST['max_capacity']);
        $car->location->longtitude = 19.039993;
        $car->location->langtitude = 50.270908;
    }

    $sdm = new SDM($db_address, $db_user, $db_password, $db_name);

    if(!$sdm->alreadyExist('cars', 'id', 'name="'.$car->name.'"')) {
 
        $tableRow = array(
            'id' => 'AUTO_INCREMENT',
            'name' => $car->name,
            'max_capacity' => $car->maxCapacity,
            'current_capacity' => $car->currentCapacity,
            'longtitude' => $car->location->longtitude,
            'langtitude' => $car->location->langtitude,
            'state' => '0');

        $sdm->insert('cars', $tableRow);

        $_SESSION["newest_car"] = $car->name;
        
    } else {
        $_SESSION["add_car_error"] = "Pojazd już istnieje w bazie. Upewnij się, że wprowadzasz poprawne dane lub sprawdź czy wspomniany pojazd nie jest już dostępny w panelu zarządzania.";
    }

    $sdm->jobDone();

    header("Location: car_configurator.php");

?>